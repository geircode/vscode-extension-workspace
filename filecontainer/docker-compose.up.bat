cd %~dp0
docker rm -f vscode-plugin-workspace-filecontainer-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up --build --remove-orphans
pause
docker exec -it vscode-plugin-workspace-filecontainer-1 /bin/bash
