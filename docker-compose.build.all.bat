cd %~dp0
docker build -f Dockerfile.nodejs -t geircode/vscode-plugin-workspace-nodejs .
docker-compose -f filecontainer/docker-compose.yml build
docker-compose -f docker-compose.yml build

if [%1]==[] goto :pause
echo Continue without pause
goto :end

:pause
pause
goto :end

:end
echo The end