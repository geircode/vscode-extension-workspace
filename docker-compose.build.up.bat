cd %~dp0

REM Create a shared volume on the Windows Host. All created vscode plugins will be stored here.
cd ..
mkdir vscode-plugin-workspace-volume

cd %~dp0
call docker-compose.build.all.bat no_pause
docker-compose -f docker-compose.yml up -d
docker exec -it --user=yeoman vscode-plugin-workspace-1 /bin/bash
pause