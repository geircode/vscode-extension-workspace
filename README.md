# Workspace for developing Visual Studio Code plugins

This workspace can create plugins using containers rather than installing a lot of stuff locally on your computer.

## Getting started

Follow the article at:

https://geircode.atlassian.net/wiki/spaces/geircode/pages/1036222465/2019-12-27+Create+VSCode+plugin+with+Windy.com+with+Azure+DevOps


