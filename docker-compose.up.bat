cd %~dp0

REM Create a shared volume on the Windows Host. All created vscode plugins will be stored here.
cd ..
mkdir vscode-plugin-workspace-volume

cd %~dp0
docker-compose -f docker-compose.yml pull
docker-compose -f docker-compose.yml up -d
REM wait for 1-2 seconds for the container to start
pause
docker exec -it --user yeoman vscode-plugin-workspace-1 /bin/bash
